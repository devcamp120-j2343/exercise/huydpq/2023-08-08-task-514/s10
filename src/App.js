
import avatar from './assets/images/48.jpg'
// CSS Stylesheet
import "./App-css-stylesheet.css";

import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="devcamp-container">
      <div>
        <img src={avatar} alt="avatar" id="devcamp-image"/>
      </div>
      <div className="devcamp-quote text-info">
        <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div className="devcamp-author text-primary">
        <span><b>Tammy Stevens</b> &nbsp; * &nbsp; Front end Developer </span>
      </div>
    </div>
  );
}


export default App;
